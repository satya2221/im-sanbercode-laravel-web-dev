<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    // function index
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.castIndex', compact('cast'));
    }

    // function create
    public function create()
    {
        return view('cast.createForm');
    }

    // function store
    public function store(Request $request)
    {
        $request->validate([
            'castName' => 'required',
            'castAge' => 'required',
            'castBio' => 'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama_cast" => $request["castName"],
            "umur" => $request["castAge"],
            "bio" => $request["castBio"],
        ]);

        return redirect()->route('cast.index')->with('success', 'Data berhasil ditambahkan');
    }
    
    // function show
    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.Castshow', compact('cast'));
    }

    // function edit
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.castEdit', compact('cast'));
    }

    // function update
    public function update(Request $request, $id)
    {
        $request->validate([
            'castName' => 'required',
            'castAge' => 'required',
            'castBio' => 'required',
        ]);

        $cast = DB::table('cast')->where('id', $id)->update([
            "nama_cast" => $request["castName"],
            "umur" => $request["castAge"],
            "bio" => $request["castBio"],
        ]);

        return redirect()->route('cast.index')->with('success', 'Data berhasil diupdate');
    }

    // function destroy
    public function destroy($id)
    {
        $cast = DB::table('cast')->where('id', $id)->delete();

        return redirect()->route('cast.index')->with('success', 'Data berhasil dihapus');
    }
    

}

