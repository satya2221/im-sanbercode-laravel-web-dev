<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/register',[AuthController::class,'register'])->name('register');
Route::post('/welcome',[AuthController::class,'welcome'])->name('welcome');
Route::get('/table',[TableController::class,'table'])->name('table');
Route::get('/data-table',[TableController::class,'dataTable'])->name('data-table');

// Cast Routes
Route::get('/cast', [CastController::class,'index'])->name('cast.index');
Route::get('/cast/create', [CastController::class,'create'])->name('cast.create');
Route::post('/cast', [CastController::class,'store'])->name('cast.store');
Route::get('/cast/{id}', [CastController::class,'show'])->name('cast.show');
Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{id}', [CastController::class,'update'])->name('cast.update');
Route::delete('/cast/{id}/destroy', [CastController::class,'destroy'])->name('cast.destroy');
