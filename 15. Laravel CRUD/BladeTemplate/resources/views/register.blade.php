@extends('Layout.master')

@section('title_page')
    Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up form</h3>
    <form method="POST" action="{{ route('welcome') }}">
        @csrf
        <label for="firstName">First Name:</label> <br>
        <input type="text" name="firstName" required> <br><br>

        <label for="lastName">Last Name:</label> <br>
        <input type="text" name="LastName" required> <br><br>

        <label for="gender">Gender:</label> <br>
        <input type="radio" name="gender" id="0">Male <br>
        <input type="radio" name="gender" id="1">Female <br><br>

        <label for="nationality">Nationality:</label> <br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesian<option>
            <option value="Malaysia">Vietnamese</option>
            <option value="Malaysia">Malaysian</option>
            <option value="Singapore">Singaporean</option>
        </select> <br><br>

        <label for="languageSpoken">Language Spoken:</label> <br>
        <input type="checkbox" name="languageSpoken" id="indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="languageSpoken" id="english"> English <br>
        <input type="checkbox" name="languageSpoken" id="other"> Other <br><br>

        <label for="bio"> Bio:</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br><br>

        <input type="submit" value="Daftar sekarang">
    </form>
@endsection