@extends('Layout.master')

@section('title_page')
    Data Cast
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            {{-- @dd($cast) --}}
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cast as $c)
                    <tr>
                        <td>{{ $c->nama_cast }}</td>
                        <td>{{ $c->umur }}</td>
                        <td>
                            <a href="{{ route('cast.show', $c->id) }}" class="btn btn-primary">Detail</a>
                            <a href="{{ route('cast.edit', $c->id) }}" class="btn btn-success">Edit</a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                Delete
                            </button>
                              <!-- Modal -->
                              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Deleting Data Cast</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      Are you sure deleting this data? It will not be restored after this action.
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <form action="{{route('cast.destroy', $c->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script src="{{ asset('/AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush