@extends('Layout.master')

@section('title_page')
    Input New Cast Data
@endsection

@section('content')
    <form method="post" action="{{ route('cast.store') }}">
        @csrf
        <div class="form-group">
            <label for="castName">Nama Cast</label>
            <input type="text" class="form-control" name="castName" placeholder="Nama Cast" required>
        </div>
        <div class="form-group">
            <label for="castAge">Usia Cast</label>
            <input type="number" class="form-control" name="castAge" placeholder="Usia Cast" min="5" max="100" required>
        </div>
        <div class="form-group">
            <label for="castBio">Biografi Cast</label>
            <textarea class="form-control" name="castBio" rows="3" required></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Input Data Cast">
    </form>
@endsection
