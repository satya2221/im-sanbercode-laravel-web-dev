@extends('Layout.master')

@section('title_page')
    Edit Data {{$cast->nama_cast}}
@endsection
    
@section('content')
<form action="{{ route('cast.update', $cast->id) }}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="castName">Nama Cast</label>
        <input type="text" class="form-control" name="castName" value="{{$cast->nama_cast}}"  required>
    </div>
    <div class="form-group">
        <label for="castAge">Usia Cast</label>
        <input type="number" class="form-control" name="castAge" value="{{ $cast->umur }}" min="5" max="100" required>
    </div>
    <div class="form-group">
        <label for="castBio">Biografi Cast</label>
        <textarea class="form-control" name="castBio" rows="3" required>{{$cast->bio}}</textarea>
    </div>

    <p>Apapun yang berubah akan tersimpan setelah disubmit dengan tombol dibawah ini</p>
    <input type="submit" class="btn btn-danger" value="Update Data Cast">

</form>
@endsection