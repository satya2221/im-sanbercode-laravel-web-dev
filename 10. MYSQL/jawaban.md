# 1. Buat Database myshop
```MySQL
CREATE DATABASE `myshop`;
```
# 2. Membuat Table pada database myshop
## Table user
```MySQL
 CREATE TABLE `users`(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY(id)
    );
```
## Table Categories
```MySQL
CREATE TABLE `categories`(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    PRIMARY KEY(id)
    );
```
## Table items
```MySQL
CREATE TABLE `items`(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    PRIMARY KEY(id),
    FOREIGN KEY(category_id) REFERENCES `categories`(id)
    );
```
# 3. Memasukkan Data pada Table
## users
```MySQL
INSERT IGNORE INTO `users`(name, email, password)
    VALUES
    ('John Doe', 'john@doe.com', 'john123'),
    ('Jane Doe', 'jane@doe.com', 'jenita123');
```
## categories
```MySQL
 INSERT IGNORE INTO `categories`(name)
    VALUES
    ('gadget'),
    ('cloth'),
    ('men'),
    ('women'),
    ('branded');
```
## items
```MySQL
INSERT IGNORE INTO `items`(name, description, price, stock, category_id)
    VALUES
    ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
    ('Uniklooh', 'baju keren dari brand ternama', 500000, 50,2),
    ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10,1);
```
# 4. Mengambil Data dari Database
## a. Mengambil data users
```MySQL
 SELECT name, email FROM `users`;
```
## b. Mengambil data items
### price > 1000000
```MySQL
 SELECT * FROM `items` WHERE price > 1000000;
```
### LIKE Watch
```MySQL
 SELECT * FROM `items` WHERE name LIKE '%Watch';
```
## c. data items join dengan kategori
```MySQL
 SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name 
 FROM `items` 
 INNER JOIN `categories` ON items.category_id = categories.id;
```
# 5. Mengubah Data dari Database
```MySQL
UPDATE `items` SET price = 2500000 WHERE id = 1;
```