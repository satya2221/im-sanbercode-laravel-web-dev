<?php 
require "animal.php";
require "frog.php";
require "ape.php";


$sheep = new Animal("shaun");

echo "Name: ".$sheep->nama."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 4
echo "Cold blooded: ".$sheep->cold_blooded. "<br>"; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "Name: ".$kodok->nama."<br>"; // "shaun"
echo "Legs: ".$kodok->legs."<br>"; // 4
echo "Cold blooded: ".$kodok->cold_blooded. "<br>"; // "no"
echo "Jump: ".$kodok->jump()."<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong->nama."<br>"; // "shaun"
echo "Legs: ".$sungokong->legs."<br>"; // 4
echo "Cold blooded: ".$sungokong->cold_blooded. "<br>"; // "no"
echo "Jump: ".$sungokong->yell()."<br>";
echo "<br>";