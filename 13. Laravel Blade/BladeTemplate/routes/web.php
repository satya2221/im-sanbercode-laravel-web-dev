<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/register',[AuthController::class,'register'])->name('register');
Route::post('/welcome',[AuthController::class,'welcome'])->name('welcome');
Route::get('/table',[TableController::class,'table'])->name('table');
Route::get('/data-table',[TableController::class,'dataTable'])->name('data-table');
