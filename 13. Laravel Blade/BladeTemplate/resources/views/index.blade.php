@extends('Layout.master')

@section('title_page')
    SanberBook
@endsection

@section('content')
    <h2>Social Media Developer Santai Berkualitas</h2>

    <p>Belajar dan Berbagi agar hidup semakin berkualitas</p>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi webiste ini</li>
        <li>Mendaftar di <a href="{{ route('register') }}">form sign up</a></li>
        <li>Selesai!</li>
    </ol>
@endsection
