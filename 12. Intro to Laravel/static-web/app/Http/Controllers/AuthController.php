<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        $first_name = $request->input('firstName');
        $last_name = $request->input('LastName');
        return view('welcome', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
